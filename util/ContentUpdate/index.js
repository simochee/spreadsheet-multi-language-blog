const GoogleSpreadsheet = require('google-spreadsheet');
const async = require('async');
const _ = require('lodash');
const fs = require('fs');

function ContentUpdate (args) {
  this.SPREADSHEET_KEY = args.spreadsheet_key;
  this.CREDS = JSON.parse(fs.readFileSync(args.creds_json));
  this.SHEET_NAME = args.sheet_name;
  this.OUTPUT_DIR = args.output_dir;
  
  this.doc = new GoogleSpreadsheet(this.SPREADSHEET_KEY);

  this.fetch = () => {
    return new Promise((resolve, reject) => {
      async.waterfall([
        // 認証する
        (step) => {
          this.doc.useServiceAccountAuth(this.CREDS, step);
        },
        // ワークシートを取得する
        (step) => {
          this.doc.getInfo((err, info) => {
            step(err, info.worksheets);
          });
        },
        // 行を取得する
        (worksheets, step) => {
          const sheet = worksheets.find(sheet => sheet.title === this.SHEET_NAME)

          sheet.getRows({
            orderby: 'date'
          }, (err, rows) => {
            step(err, rows.filter(row => row.status === '公開'));
          });
        },
        // 行を加工する
        (rows, step) => {
          const posts = rows.map(row => ({
            date: row.date,
            author: row.auther,
            ja: {
              title: row['title-ja'],
              content: row['content-ja']
            },
            en: {
              title: row['title-en'],
              content: row['content-en']
            }
          }));

          step(null, posts);
        }
      ], (err, posts) => {
        if (err) {
          throw new Error(err);
        }
        fs.writeFileSync(`${this.OUTPUT_DIR}/posts.js`, `module.exports=${JSON.stringify(posts)}`);
        console.log(`🎉 ${this.OUTPUT_DIR}/にposts.jsを書き出しました`);
      });
    });
  }
}

module.exports = ContentUpdate;