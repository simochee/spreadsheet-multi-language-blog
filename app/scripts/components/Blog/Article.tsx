import * as React from 'react';
import * as moment from '../../../../node_modules/moment/moment';
import * as ReactMarkdown from 'react-markdown';

export interface Props {
  date: string;
  author: string;
  title: string;
  content: string;
}
export interface State {}

class ArticleComponent extends React.Component<Props, State> {
  render () {
    const {
      date,
      author,
      title,
      content
    } = this.props;
    
    return (
      <article>
        <header>
          <h1>{title}</h1>
          <time>{moment(date).format('MMMM Do, YYYY')}</time>
        </header>
        <body>
          <ReactMarkdown
            source={content}
          />
        </body>
      </article>
    );
  }
}

export const Article = ArticleComponent;