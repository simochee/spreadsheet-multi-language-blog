import * as Lockr from 'lockr';
import * as _ from 'lodash';

export const LANG = {
  JA: 'ja',
  EN: 'en'
};

export interface Types {
  language: string;
}

class UserAgent implements Types {
  language;

  constructor () {
    this.language = this.getLanguage();
  }

  getLanguage () {
    const userLanguage = Lockr.get('user_language', '');

    if (LANG[userLanguage]) {
      return LANG[userLanguage];
    }

    if (!_.has(navigator, 'languages')) {
      return LANG.JA;
    }
    
    const language = navigator.languages[0];
    return /ja/.test(language) ? LANG.JA : LANG.EN;
  }
}

export const userAgent = new UserAgent();
