import * as React from 'react';

import { Blog } from './Blog';

export interface Props {}
export interface State {}

export class Root extends React.Component<Props, State> {
  render () {
    return (
      <div>
        <h1>hello react</h1>
        <Blog />
      </div>
    );
  }
}