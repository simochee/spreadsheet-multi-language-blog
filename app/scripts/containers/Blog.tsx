import * as React from 'react';

import * as posts from '../constants/posts';
import { userAgent } from '../utilities/UserAgent';
import { Article } from '../components/Blog/Article';

export interface Props {}
export interface State {}

export class Blog extends React.Component<Props, State> {
  render () {
    const lang = userAgent.language;
    
    return (
      <div className="container">
        { posts.map((post, i) => (
          <Article
            key={i}
            date={post.date}
            author={post.author}
            title={post[lang].title}
            content={post[lang].content}
          />
        )) }
      </div>
    );
  }
}